# Census data classification with R

* [info](https://archive.ics.uci.edu/ml/datasets/Census-Income+%28KDD%29)
* [data](https://archive.ics.uci.edu/ml/machine-learning-databases/census-income-mld/census.tar.gz)

The data is a set of 41 variables describing 200,000 individuals in the USA in 1994 & 1995.

## Introduction, problem & goal

The goal of this project is to classify individuals into 2 classes: low incomes & high incomes. The threshold value is $50,000. The individuals are described with 40 variables.

To assess the performance of our result, the misclassification error rate as been chosen. We have 2 classes and they are not equally distributed in our data: the low class is 20 times more present than the high class. Then the simplest classifier which minimizes the misclassification error is to classify all data into the low class. Thus, we have to beat an error rate of around 5%.

## Process summary

* Study variables independently, against the outcome, and their correlations
* Recast some variables
* Aggregates quantitative variable levels
* Discretize quantitative variables
* Variable selections
* First test of classification with logistic regression on MCA principal components
* Iterations of classifications with trees
* Classification with random forest

## Exploration and data pre-processing

see code.

* Quantitative variables histograms:
![Quantitative variables histograms](https://gitlab.com/aq1/census-predict/raw/master/viz/quantitative_variable_histogram.png?inline=false)
* Discretized continuous variables histograms:
![Discretized continuous variables histograms](https://gitlab.com/aq1/census-predict/raw/master/viz/discreetized_continuous_variables_histograms.png?inline=false)
* Stargazer auto analysis:
![Stargazer](https://gitlab.com/aq1/census-predict/raw/master/viz/stargazer.png?inline=false)
* Continuous variable without zeros:
![Continuous variable without zeros](https://gitlab.com/aq1/census-predict/raw/master/viz/continuous_variable_without_zero.png?inline=false)
* Qualitative variables standalone:
![Qualitative variables standalone](https://gitlab.com/aq1/census-predict/raw/master/viz/qualitative_variables_standalone.png?inline=false)
* Many levels qualitative variable:
![Many levels qualitative variable](https://gitlab.com/aq1/census-predict/raw/master/viz/many_levels_qualitative_variable.png?inline=false)
* Quantitative variables per outcome:
![Quantitative variables per outcome](https://gitlab.com/aq1/census-predict/raw/master/viz/quantitative_variable_per_outcome.png?inline=false)
* Quantitative variables discretization:
![Quantitative variables discretization](https://gitlab.com/aq1/census-predict/raw/master/viz/discretization.png?inline=false)
* Normalization:
![Normalization](https://gitlab.com/aq1/census-predict/raw/master/viz/qqplot.png?inline=false)
* Outcome per qualitative variables:
![Outcome per qualitative variables](https://gitlab.com/aq1/census-predict/raw/master/viz/outcome_per_qualitative_variables.png?inline=false)
* Qualitative variables aggregation:
![Qualitative variables aggregation](https://gitlab.com/aq1/census-predict/raw/master/viz/levels_aggregations.png?inline=false)
* Quantitative variables selection:
![Quantitative variables selection](https://gitlab.com/aq1/census-predict/raw/master/viz/eta2.png?inline=false)
* Qualitative variables selection:
![Qualitative variables selection](https://gitlab.com/aq1/census-predict/raw/master/viz/cramer_v.png?inline=false)
* Correlations:
![Degrees distribution](https://gitlab.com/aq1/census-predict/raw/master/viz/correlations_1.png?inline=false)
zoomed:
![Degrees distribution](https://gitlab.com/aq1/census-predict/raw/master/viz/correlations_2.png?inline=false)

## Logistic regression on MCA principal components

see code.

## Simple tree classification

see code. ROC curve:
![ROC curve](https://gitlab.com/aq1/census-predict/raw/master/viz/roc_curve_simple_tree.png?inline=false)

## Unpruned tree classification

see code. ROC curve:
![ROC curve](https://gitlab.com/aq1/census-predict/raw/master/viz/roc_curve_upruned_tree.png?inline=false)

## Tree to minimized misclassification error

see code. ROC curve:
![ROC curve](https://gitlab.com/aq1/census-predict/raw/master/viz/roc_curve_pruned_tree.png?inline=false)

## Random forest

see code. ROC curve:
![ROC curve](https://gitlab.com/aq1/census-predict/raw/master/viz/roc_curve_forest.png?inline=false)

# Conclusion

* Preprocessing of data could be improved: multiple tests could be run to see the utility of each variable provided to the model. Also trying different cuts in discretization could yield improvements.
* The logistic regression model was just here to play.
* The generated trees were yielding decent results with the default operating points. I am not sure that finding the optimal cutoff which minimizes the misclassification error was correct.
* Random forest is not surprisingly the best model as it contains some sort of selection on the variables from within its process.
