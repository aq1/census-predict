# Census report

# Load data

load("data_train.rda")
census <- data_train
load("data_test.rda")
census.test <- data_test
ls()
# [1] "census"      "census.test" "data_test"   "data_train" 
str(census)

# Recode variables
census$age <- as.numeric(census$age)
census$`wage per hour` <- as.numeric(census$`wage per hour`)
census$`capital gains` <- as.numeric(census$`capital gains`)
census$`capital losses` <- as.numeric(census$`capital losses`)
census$`from stocks` <- as.numeric(census$`from stocks`)
census$`weeks worked in year` <- as.numeric(census$`weeks worked in year`)
census$`num persons worked for employer` <-
  as.numeric(census$`num persons worked for employer`)
census$`detailed industry recode` <- as.factor(census$`detailed industry recode `)
census$`detailed industry recode ` <- NULL
census$`detailed occupation recode` <- as.factor(census$`detailed occupation recode`)
census$`business or self employed` <- as.factor(census$`business or self employed`)
census$`veterans benefits` <- as.factor(census$`veterans benefits`)
census$`year` <- as.factor(census$`year`)

# Number of lines & variables
dim(census)
# [1] 199526     41

# Type of variables
table(sapply(census, class))
# 
#  factor integer numeric 
#      29       7       5 

# Name list of variables by type
var.factor<-which(sapply(census, class) == "factor")
var.numeric<-which(sapply(census,class)=="numeric"|sapply(census,class)=="integer")

# Missing data
sum(is.na(census))
# [1] 415836

# Analysis of variables 1 by 1
install.packages("stargazer")
library(stargazer)
stargazer(census,
          summary.stat=c("n","min","p25","median","mean","p75","max","sd"),
         type = "text")
# => wage per hours, capital gains & losses, from stocks:
# Pxtl(75) = 0 
# lots of zero
# could be zeros in place of missing data 

# Num persons worked for employer & age
varbarplot <- c("num persons worked for employer", "age")
par(mfrow=c(1,2))
mapply( census[,varbarplot],
  FUN=function(xx,name){barplot(table(xx),main=name)},
  name=varbarplot)

# * num persons worked for employer is bipolar 0 or 6
# * Age has a tail on the right => log transformation

# Wage per hour

par(mfrow=c(1,1))
table(census$`wage per hour`)["0"] / length(census$`wage per hour`)
#         0 
# 0.9438219 
# 94% of the values are zeros

# lets plot non zeros
hist( census$`wage per hour`[census$`wage per hour` != 0 & census$`wage per hour` < 4000],
    xlab = "wage/hour (no zéros)",
    main = "wage per hour")
# Wage per hour without zeros has a long tail on the right
# => logarithme transformation

# Capital gains & losses non zero
par(mfrow=c(1,3))
hist( census$`capital gains`[census$`capital gains` != 0],
    main = "capital gains",
    xlab = "capital gains (non zero)")
# bipolar, could be discretized
hist( census$`capital losses`[census$`capital losses` != 0],
    main = "capital losses",
    xlab = "capital losses (non zero)")
# capital losses looks like a bell curve
hist( census$`from stocks`[census$`from stocks` != 0],
    main = "from stocks",
    xlab = "from stocks (non zero)")
# from stocks is rapidely decreasing like an exponential



# Weeks worked in year
par(mfrow=c(1,1))
barplot(table(census$`weeks worked in year`), main="weeks worked in year")
# => People works or don't work: 0 or 52 weeks
# or some zero could be some missing values
# We could take 3 classes


# Box plots

install.packages("car")
library(car)
dev.off()
par(mfrow=c(2,4))
mapply( census[,var.numeric],
  FUN=function(xx,name){Boxplot(xx,main=names(census)[name],id.n=2,ylab="")},
  name=var.numeric)

# => Same conclusion as before: somes with too much zeros. Somes bi-polar (work or don't work). Some wage per hour are weird.

# Qualitative variables


# Qualitatives variables, factor types, with less than 8 levels

var.factor.few_levels <- which(
  sapply(census, class) == "factor" & sapply(census, nlevels) <= 8)
length(var.factor.few_levels)
var.factor.few_levels
# [1] 20
dev.off()
par(mfrow=c(4,5))
mapply( census[,var.factor.few_levels],
  FUN=function(xx,name){
    barplot(table(xx),
            main=name,
            horiz = TRUE,
            las = 2)
  }, name=names(var.factor.few_levels))
# variable with joinable levels:
# - marital status
# - reason for unemployment
# - full or part time employment
# - tax filler stats
# - detailed household summary
# - migration code change
# - family members under 18
# - citizen ship

# year could be ignored ?


# Qualitatives variables, factor types, with many levels
var.factor.many_levels <- which(
  sapply(census, class) == "factor" & sapply(census, nlevels) > 8)
var.factor.many_levels
options(scipen=6)
dev.off()
par(mfrow=c(1,1), mar=c(5.1,15,4.1,2.1))
barplot( table(census$`class of worker`),
          main="class of worker",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
barplot( table(census$`detailed occupation recode`),
          main="detailed occupation recode",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
# Those are the jobs. Certainly very good estimators. Not easily groupable ?
barplot( table(census$`education`),
          main="education",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
barplot( table(census$`major industry code`),
          main="major industry code",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
# The industry of work. Certainly very good estimators. Not easily groupable ?
barplot( table(census$`major occupation code`),
          main="major occupation code",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
# not clear categories
barplot( table(census$`hispanic origin`),
          main="hispanic origin",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
# could be merge with countries & races
barplot( table(census$`state of previous residence`),
          main="state of previous residence",
          horiz = TRUE,
          mar = c(2,10,2,2),
          cex.names=0.4,
          col = "mistyrose",
          las = 2)
# Not usefull I think
barplot( table(census$`detailed household and family stat`),
          main="detailed household and family stat",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
# should be interesting to group
barplot( table(census$`migration code-change in msa`),
          main="migration code-change in msa",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
# Metropolitan Statistical Areas. Not easily minable.
barplot( table(census$`migration code-move within reg`),
          main="migration code-move within reg",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
# Not easily minable.
barplot( table(census$`country of birth father`),
          main="country of birth father",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          cex.names=0.5,
          las = 2)
# could be merge with countries & races
barplot( table(census$`country of birth mother`),
          main="country of birth mother",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          cex.names=0.5,
          las = 2)
# could be merge with countries & races
barplot( table(census$`country of birth self`),
          main="country of birth self",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          cex.names=0.5,
          las = 2)
# could be merge with countries & races
barplot( table(census$`detailed industry recode`),
          main="detailed industry recode",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          cex.names=0.5,
          las = 2)
# Kinda the same as job


# variable with joinable levels:
# - class of worker
# - education
# - detailed household and family stat

# Retour sur les variables Quantitatives

wage.per.hour.non.zero  <- census$`wage per hour`[census$`wage per hour` != 0]
wage.per.hour.non.zero.log <-log(wage.per.hour.non.zero)
par(mfrow=c(1,2))
qqPlot(wage.per.hour.non.zero)
qqPlot(wage.per.hour.non.zero.log)
census$`wage per hour` <- log(census$`wage per hour`)
# => better linearity of wage per hour
# NNNNOOOOOOOOO !

# Discretization not finished
install.packages("discretization")
library(discretization)
res.mdlp <- mdlp(census[,c("age", "age")])
str(res.mdlp$Disc.data)

# The most discriminant quantitative variables
install.packages("BioStatR")
library(BioStatR)
res.eta2 <- sapply(census[,var.numeric], eta2, y = census$outcome)
res.eta2 <- sort(res.eta2)
par(mar=c(5, 15, 4, 2) + 0.1)
barplot(res.eta2,
        horiz = TRUE,
        las=2,
        xlab=expression(eta^2))
# We can remove Wage per hour

# Combined analysis of qualitative variables with few levels
par(mfrow=c(4,5))
mapply( census[,var.factor.few_levels],
  FUN=function(xx,name){
    tmp <- table(xx,census$outcome)
    tmp <- tmp/rowSums(tmp)
    barplot(
      tmp[,"+50000"],
      main=name,
      horiz = TRUE,
      las=2,
      cex.names=0.4,
      col = "mistyrose",
      xlim=c(0,0.2)
    )
  }, name=names(var.factor.few_levels))

# Candidate variables to remove
candidates <- c("enroll in edu inst last wk",
  "marital stat",
  "member of a labor union",
  "region of previous residence",
  "migration code-change in reg",
  "migration prev res in sunbelt",
  "year")
var.factor.few_levels[candidates]
dev.off()
par(mfrow=c(3,3), mar = c(2,10,2,2))
mapply( census[,var.factor.few_levels[candidates]],
  FUN=function(xx,name){
    tmp <- table(xx,census$outcome)
    tmp <- tmp/rowSums(tmp)
    barplot(
      tmp[,"+50000"],
      main=name,
      horiz = TRUE,
      las=2,
      cex.names=0.6,
      col = "mistyrose",
      xlim=c(0,0.2)
    )
  }, name=names(var.factor.few_levels[candidates]))

# Quantitative variables with few levels to keep
var.factor.few_levels.to.remove <- var.factor.few_levels[var.factor.few_levels %in% var.factor.few_levels[candidates]]
census <- census[- var.factor.few_levels.to.remove]
str(census)
dim(census)



# Class of worker
dev.off()
par(mfrow=c(1,1), mar=c(5.1,15,4.1,2.1))
xx <- census$`class of worker`
tmp <- table(xx,census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per Class of worker",
  horiz = TRUE,
  las=2,
  cex.names=0.8,
  col = "mistyrose",
  xlim=c(0,0.4)
)
levels(census$`class of worker`)
levels(census$`class of worker`) <- c(" Federal government", " State and Local government",
" Other", " Self-employed-not incorporated or Private", " Self-employed-incorporated",
" Self-employed-not incorporated or Private", " State and Local government",
" Other")


# Job
dev.off()
par(mfrow=c(1,1))
xx <- census$`detailed occupation recode`
tmp <- table(xx,census$outcome)
tmp <- tmp/rowSums(tmp)
str(tmp[, 2] < 0.1)
str(dimnames(tmp)[1]$xx)
levels.to.remove <- dimnames(tmp)[1]$xx[tmp[, 2] < 0.1]
levels(census$`detailed occupation recode`)
levels.to.remove.indexes <- match(levels.to.remove, levels(census$`detailed occupation recode`))
levels.to.remove.indexes
levels(census$`detailed occupation recode`) <- replace(levels(census$`detailed occupation recode`), levels.to.remove.indexes, "Other")
length(levels(census$`detailed occupation recode`))
barplot(
  tmp[,"+50000"],
  main="Outcome per detailed occupation recode",
  horiz = TRUE,
  las=2,
  cex.names=0.4,
  col = "mistyrose",
  xlim=c(0,0.8)
)

aggregate_levels_under_threashold_list <- function(dataframe, variable, target, threashold, replacement) {
  original_levels <- levels(dataframe[[variable]])
  # Calculate the proportions
  tmp_table <- table(dataframe[[variable]], dataframe[[target]])
  tmp_table<- tmp_table/rowSums(tmp_table)
  # levels with proportions lower than the level
  levels_with_low_values <- tmp_table[, 2] < threashold
  levels_to_aggregate <- original_levels[levels_with_low_values]
  levels_to_aggregate_indexes <- match(levels_to_aggregate, original_levels)
  list_of_levels <- replace(original_levels, levels_to_aggregate_indexes, replacement)
  return(list_of_levels)
}

# education
dev.off()
par(mfrow=c(1,1), mar=c(5,25,2,2))
barplot( table(census$`education`),
          main="education",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
tmp <- table(census$education,census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per education",
  horiz = TRUE,
  las=2,
  cex.names=0.4,
  col = "mistyrose",
  xlim=c(0,0.8)
)
levels(census$education) <- aggregate_levels_under_threashold_list(census, "education", "outcome", 0.05, "Lower than high school graduate")
tmp <- table(census$education, census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per education",
  horiz = TRUE,
  las=2,
  cex.names=0.4,
  col = "mistyrose",
  xlim=c(0,0.8)
)


# Major industry code
dev.off()
par(mfrow=c(1,1), mar=c(5,25,2,2))
barplot( table(census$`major industry code`),
          main="major industry code",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
tmp <- table(census$"major industry code", census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per marjor industry code",
  horiz = TRUE,
  las=2,
  cex.names=0.7,
  col = "mistyrose",
  xlim=c(0,0.8)
)
levels(census$"major industry code") <- aggregate_levels_under_threashold_list(census, "major industry code", "outcome", 0.1, "industries with lower outcomes")
tmp <- table(census$"major industry code", census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per marjor industry code",
  horiz = TRUE,
  las=2,
  cex.names=0.7,
  col = "mistyrose",
  xlim=c(0,0.8)
)


# Job
dev.off()
par(mfrow=c(1,1), mar=c(5,25,2,2))
barplot( table(census$`major occupation code`),
          main="major occupation code",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
tmp <- table(census$"major occupation code", census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per marjor occupation code",
  horiz = TRUE,
  las=2,
  cex.names=0.7,
  col = "mistyrose",
  xlim=c(0,0.8)
)
levels(census$"major occupation code") <- aggregate_levels_under_threashold_list(census, "major occupation code", "outcome", 0.08, "jobs with lower outcomes")
tmp <- table(census$"major occupation code", census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per marjor occupation code",
  horiz = TRUE,
  las=2,
  cex.names=0.7,
  col = "mistyrose",
  xlim=c(0,0.8)
)


# Hispanic origin
barplot( table(census$`hispanic origin`),
          main="hispanic origin",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
tmp <- table(census$"hispanic origin", census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per hispanic origin",
  horiz = TRUE,
  las=2,
  cex.names=0.7,
  col = "mistyrose",
  xlim=c(0,0.8)
)
# => could be removed ???


# State of previous residence
barplot( table(census$`state of previous residence`),
          main="state of previous residence",
          horiz = TRUE,
          mar = c(2,10,2,2),
          cex.names=0.4,
          col = "mistyrose",
          las = 2)
tmp <- table(census$"state of previous residence", census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per state of previous residence",
  horiz = TRUE,
  las=2,
  cex.names=0.7,
  col = "mistyrose",
  xlim=c(0,1)
)
# Does changing state means something about the outcome ?
# No => To remove !


# deailed household & family stat
barplot( table(census$`detailed household and family stat`),
          main="detailed household and family stat",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
tmp <- table(census$"detailed household and family stat", census$outcome)
tmp <- tmp/rowSums(tmp)
barplot(
  tmp[,"+50000"],
  main="Outcome per detailed household and family stat",
  horiz = TRUE,
  las=2,
  cex.names=0.7,
  col = "mistyrose",
  xlim=c(0,0.2)
)
levels(census$"detailed household and family stat") <- aggregate_levels_under_threashold_list(census, "detailed household and family stat", "outcome", 0.02, "Others with very low outcome")


plot_vs_outcome <- function(variable, xlim = 1, cex.names = 1) {
  tmp <- table(census[[variable]], census$outcome)
  tmp <- tmp/rowSums(tmp)
  barplot(
    tmp[,"+50000"],
    main=paste("Outcome per", variable),
    horiz = TRUE,
    las=2,
    cex.names=cex.names,
    col = "mistyrose",
    xlim=c(0,xlim)
  )
}

# migration code change
barplot( table(census$`migration code-change in msa`),
          main="migration code-change in msa",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
plot_vs_outcome("migration code-change in msa", xlim = 1, cex.names = 1)
# => This is a variable to ignore

barplot( table(census$`migration code-move within reg`),
          main="migration code-move within reg",
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          las = 2)
plot_vs_outcome("migration code-move within reg", xlim = 1, cex.names = 1)
# => This is a variable to ignore


table_plot <- function(variable) {
  barplot( table(census[[variable]]),
          main=variable,
          horiz = TRUE,
          mar = c(2,10,2,2),
          col = "mistyrose",
          cex.names=0.5,
          las = 2)
}

# country of birth father
table_plot("country of birth father")
table_plot("country of birth mother")
plot_vs_outcome("country of birth father", xlim = 0.2, cex.names = 1)
plot_vs_outcome("country of birth mother", xlim = 0.2, cex.names = 1)
# lets reduce the number of levels
levels(census$"country of birth father") <- aggregate_levels_under_threashold_list(census, "country of birth father", "outcome", 0.035, "Others with low outcome")
plot_vs_outcome("country of birth father", xlim = 0.2, cex.names = 1)
length(levels(census$"country of birth father"))
# [1] 28
levels(census$"country of birth mother") <- aggregate_levels_under_threashold_list(census, "country of birth mother", "outcome", 0.035, "Others with low outcome")
plot_vs_outcome("country of birth mother", xlim = 0.2, cex.names = 1)
length(levels(census$"country of birth mother"))
# [1] 27

table_plot("country of birth self")
plot_vs_outcome("country of birth self", xlim = 0.2, cex.names = 1)
length(levels(census$"country of birth self"))
# [1] 42
levels(census$"country of birth self") <- aggregate_levels_under_threashold_list(census, "country of birth self", "outcome", 0.035, "Others with low outcome")
length(levels(census$"country of birth self"))
# [1] 30
plot_vs_outcome("country of birth self", xlim = 0.2, cex.names = 1)


# detailed industry recode
table_plot("detailed industry recode")
plot_vs_outcome("detailed industry recode", xlim = 0.5, cex.names = 0.3)
length(levels(census$"detailed industry recode"))
# [1] 52
levels(census$"detailed industry recode") <- aggregate_levels_under_threashold_list(census, "detailed industry recode", "outcome", 0.12, "Others with low outcome")
length(levels(census$"detailed industry recode"))
# [1] 27
plot_vs_outcome("detailed industry recode", xlim = 0.5, cex.names = 0.6)
